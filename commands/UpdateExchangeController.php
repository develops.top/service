<?php

namespace app\commands;

use yii\console\Controller;

// /usr/local/php73/bin/php > yii update-exchange/sync
class UpdateExchangeController extends Controller
{
    public function actionSync()
    {
        exchangeService()->sync();
    }
}
