<?php

namespace app\repository\search;

use app\modules\admin\models\Translate;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use Yii;

class IndexSearch extends Model
{
    public $dynamicFields = [];
    public $columns = [];
    public $phrase;

    public function __construct($config = [])
    {
        foreach (languageService()->getList() as $id => $name) {
            $this->dynamicFields[$name] = $id;
        }
        $this->setColumns();
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['dynamicFields', 'columns'], 'safe'],
            [['phrase'], 'string'],
        ];
    }

    public function search($params)
    {

        $query = Translate::find();
        $query->joinWith('language');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!empty($this->dynamicFields)) {
            foreach ($this->dynamicFields as $name => $id) {
                $dataProvider->sort->attributes[$name] = [
                    'asc' => ['phrase' => SORT_ASC],
                    'desc' => ['phrase' => SORT_DESC],
                ];
            }
        }

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->groupBy('phrase_number');

        $query->andFilterWhere(['like', 'phrase', $this->phrase]);

//        if (!empty($params['sort'])) {
////            $lang = languageService()->languageRepository->findOneByParams(['name' => str_replace('-', '', $params['sort'])]);
//            if (stripos($params['sort'], '-') !== false) {
//                $query->orderBy(
//                    [
//                        new \yii\db\Expression('phrase IS NULL OR phrase = "" ASC')
//                    ]
//                );
//            } else {
//                $query->orderBy(
//                    [
//                        new \yii\db\Expression('phrase IS NULL OR phrase = "" DESC')
//                    ]
//                );
//            }
//
//        }


//        echo '<pre>';
//        print_r($query->createCommand()->getRawSql());
//        die;

//        echo '<pre>';
//        print_r($params['sort']);
//        die;

        return $dataProvider;
    }

    public function setColumns()
    {
        array_push($this->columns, [
            'attribute' => '',
            'format' => 'raw',
            'value' => function($model) {
                return Html::checkbox('phrases', '', ['class' => 'phrases', 'data-phrase_number' => $model->phrase_number]);
            }
        ]);
        foreach (languageService()->getList() as $langId => $langName) {
            array_push($this->columns, [
                    'attribute' => $langName,
                    'format' => 'raw',
                    'value' => function($model) use ($langId) {
                        $model = translateService()->getTranslate($langId, $model->phrase_number);
                        $content = isset($model->phrase) ? $model->phrase : '';
                        if (isset($model->phrase_description) && $model->phrase_description) {
                            $content .= '<span class="descriptionIcon" data-trigger="hover" data-content="'. $model->phrase_description .'">
                                            <i class="fa-solid fa-circle-question"></i>
                                        </span>';
                        }
                        return $content;
                    },
                ]
            );
        }
        array_push($this->columns, [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{edit}',
            'headerOptions' => ['style' => 'min-width:180px;width:180px'],
            'buttons' => [
                'edit' => function($url, $model, $key) {
                    return Html::a(Yii::t('app', 'Редактировать'), ['/admin/home/edit', 'phrase' => $model->phrase_number], ['class' => 'btn btn-primary']);
                }
            ],
        ]);
    }
}