<?php

namespace app\repository;

use app\modules\exchange\models\Currency;
use RuntimeException;

class CurrencyRepository extends BaseRepository
{
    public function query()
    {
        return Currency::find();
    }

    public function save(Currency $model)
    {
        if (!$model->save()) {
            throw new RuntimeException('Saving error.' . __LINE__ );
        }
        return $model;
    }

    public function batchInsert($data)
    {
        \Yii::$app->db->createCommand()->batchInsert(Currency::tableName(),
            [
                'r030',
                'txt',
                'rate',
                'cc',
                'exchangedate',
            ],
            $data
        )->execute();
    }

    public function clearData()
    {
        \Yii::$app->db->createCommand()->truncateTable(Currency::tableName())->execute();
    }
}