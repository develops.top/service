<?php

namespace app\repository\interfaces;

interface BaseQueryInterface
{
    public function query();
    public function findOneByParams($params);
    public function findAllByParams($params);
}