<?php

namespace app\repository;

use app\repository\interfaces\BaseQueryInterface;

abstract class BaseRepository implements BaseQueryInterface
{
    public function findOneByParams($params)
    {
        return $this->query()->where($params)->limit(1)->one();
    }

    public function findAllByParams($params)
    {
        return $this->query()->where($params)->all();
    }

    public function findById($id)
    {
        return $this->findOneByParams(['id' => $id]);
    }

    public function findAll()
    {
        return $this->query()->all();
    }

    public function isExistByParams($params)
    {
        return $this->query()->where($params)->limit(1)->exists();
    }

    public function getCountByParams($params)
    {
        return $this->query()->where($params)->count();
    }

    public function getCount()
    {
        return $this->query()->count();
    }

    public function updateByParamsBase($tableName, $params, $conditions)
    {
        \Yii::$app->db->createCommand()->update($tableName, $params, $conditions)->execute();
    }

    public function deleteByParams($tableName, $conditions)
    {
        return \Yii::$app->db->createCommand()->delete($tableName, $conditions)->execute();
    }
}