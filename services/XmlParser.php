<?php

namespace app\services;

use yii\base\BaseObject;
use yii\httpclient\ParserInterface;

class XmlParser extends BaseObject implements ParserInterface
{

    public function parse($response)
    {
        $contentType = $response->getHeaders()->get('content-type', '');
        if (preg_match('/charset=(.*)/i', $contentType, $matches)) {
            $encoding = $matches[1];
        } else {
            $encoding = 'UTF-8';
        }

        $dom = new \DOMDocument('1.0', $encoding);
        $dom->loadXML($response->getContent(), LIBXML_NOCDATA);
        return $this->convertXmlToArray(simplexml_import_dom($dom->documentElement));
    }

    protected function convertXmlToArray($xml)
    {
        if (is_string($xml)) {
            $xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        }
        $result = (array) $xml;
        foreach ($result as $key => $value) {
            if (!is_scalar($value)) {
                $result[$key] = $this->convertXmlToArray($value);
            }
        }
        return $result;
    }
}