<?php

namespace app\services;

use app\repository\CurrencyRepository;
use Yii;
use yii\httpclient\Client;
use \yii\httpclient\Response;

class ExchangeService
{
    public $xmlParser;
    public $currencyRepository;

    public function __construct()
    {
        $this->xmlParser = new XmlParser();
        $this->currencyRepository = new CurrencyRepository();
    }

    public function sync()
    {
        $data = $this->getData();
        if (isset($data['currency']) && !empty($data['currency'])) {
            $this->convertExchangedate($data['currency']);
            exchangeService()->currencyRepository->clearData();
            exchangeService()->currencyRepository->batchInsert($data['currency']);
        }
    }

    public function getData() : array
    {
        $response = $this->getResponse();
        if ($this->isValidResponse($response)) {
            return $this->xmlParser->parse($response);
        }
        return [];
    }

    private function convertExchangedate(&$data)
    {
        foreach ($data as &$item) {
            $item['exchangedate'] = date('Y-m-d', strtotime($item['exchangedate']));
        }
    }

    private function getResponse()
    {
        $client = new Client();
        return $client->createRequest()
            ->setMethod('GET')
            ->setUrl(Yii::$app->params['parseUrl'])
            ->send();
    }

    private function isValidResponse(Response $response) : bool
    {
        if (!$response->isOk) {
            // TODO send logs
            return false;
        }
        if ($response->getFormat() !== 'xml') {
            // TODO send logs
            return false;
        }
        return true;
    }
}