<?php

return [
    'exchange' => [
        'class' => 'app\modules\exchange\Module',
    ],
    'api' => [
        'class' => 'app\modules\api\Module',
    ]
];