<?php

namespace app\modules\exchange\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\exchange\models\Currency as CurrencyModel;

/**
 * Currency represents the model behind the search form of `app\modules\exchange\models\Currency`.
 */
class CurrencySearch extends CurrencyModel
{
    public $page;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'r030'], 'integer'],
            [['txt', 'exchangedate'], 'safe'],
            [['rate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = CurrencyModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'r030' => $this->r030,
            'rate' => $this->rate,
            'exchangedate' => $this->exchangedate,
        ]);

        $query->andFilterWhere(['like', 'txt', $this->txt]);

        return $dataProvider;
    }
}
