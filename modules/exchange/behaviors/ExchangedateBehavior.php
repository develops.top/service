<?php

namespace app\modules\exchange\behaviors;

use yii\base\Behavior;
use app\modules\exchange\models\Currency;

class ExchangedateBehavior extends Behavior {

    public $event;

    public function events()
    {
        return [
            Currency::EVENT_BEFORE_INSERT => 'checkExchangedate',
            Currency::EVENT_BEFORE_UPDATE => 'checkExchangedate',
        ];
    }

    public function checkExchangedate()
    {
        if (!empty($this->owner->exchangedate)) {
            $this->owner->exchangedate = date('Y-m-d', strtotime($this->owner->exchangedate));
        }
    }
}